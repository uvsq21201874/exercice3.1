
public class Compte {
	private int solde;
	
	public Compte(){
		this.solde=100;
	}
	
	public Compte(int S){
		this.solde=S;
	}
	
	
	public int getsolde(){
		return this.solde;
	}
	public void setsolde(int newSolde){
		this.solde=newSolde;
	}
	
	public int consultation(){
		return this.solde;
	}
	public void credit(int somme){
		this.setsolde(this.getsolde()-somme);
	}
	public void virement(Compte receveur, int somme){
		receveur.setsolde(receveur.getsolde()+somme);
		this.setsolde(this.getsolde()-somme);
	}
}

class Main
{
	public static void main(String[] args){
		System.out.println("test");
	}
}
